#!/bin/bash

set -e

host="$1"
shift
cmd="$@"

>&2 echo "Waiting for Postgres - Starting the postgres lookup"

until pg_isready -h "$host"; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "cmd arg: ${cmd}"
>&2 echo "Postgres is up - entering app"
exec $cmd

