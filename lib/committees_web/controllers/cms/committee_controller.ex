defmodule CommitteesWeb.CMS.CommitteeController do
  use CommitteesWeb, :controller

  alias Committees.CMS
  alias Committees.CMS.Committee
  alias Committees.Accounts

  def index(conn, _params) do
    committees = CMS.list_committees()
    render(conn, "index.html", committees: committees)
  end

  def new(conn, _params) do
    changeset = CMS.change_committee(%Committee{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"committee" => committee_params}) do
    case CMS.create_committee(committee_params) do
      {:ok, committee} ->
        conn
        |> put_flash(:info, "Komitee erstellt")
        |> redirect(to: cms_committee_path(conn, :show, committee))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    committee = CMS.get_committee!(id)
    render(conn, "show.html", committee: committee)
  end

  def edit(conn, %{"id" => id}) do
    committee = CMS.get_committee!(id)
    changeset = CMS.change_committee(committee)
    render(conn, "edit.html", committee: committee, changeset: changeset)
  end

  def update(conn, %{"id" => id, "committee" => committee_params}) do
    committee = CMS.get_committee!(id)

    case CMS.update_committee(committee, committee_params) do
      {:ok, committee} ->
        conn
        |> put_flash(:info, "Komitee gespeichert")
        |> redirect(to: cms_committee_path(conn, :show, committee))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", committee: committee, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    committee = CMS.get_committee!(id)
    {:ok, _committee} = CMS.delete_committee(committee)

    conn
    |> put_flash(:info, "Komitee gelöscht")
    |> redirect(to: cms_committee_path(conn, :index))
  end

  def new_user(conn, %{"id" => id}) do
    committee = CMS.get_committee!(id)
    users = Accounts.list_users() |> Enum.map(fn user -> {user.name, user.id} end)

    render(conn, "new_user.html", committee: committee, users: users)
  end

  def create_user(conn, %{"id" => id, "user" => %{"user_id" => user_id}}) do
    committee = CMS.get_committee!(id)
    user = Accounts.get_user!(user_id)

    case CMS.add_user_to_committee(user, committee) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Nutzer erfolgreich eingeschrieben")
        |> redirect(to: cms_committee_path(conn, :show, committee.id))
      _ ->
        conn
        |> put_flash(:error, "Nutzer nicht gefunden. Bitte probier es noch einmal.")
        |> redirect(to: cms_committee_path(conn, :new_user, committee.id))
    end

    conn
  end

  def remove_user(conn, %{"id" => id, "user_id" => user_id}) do
    committee = CMS.get_committee!(id)
    user = Accounts.get_user!(user_id)

    cond do
      user && committee ->
        case CMS.remove_user_from_committee(user, committee) do
          {1, _} ->
            conn
            |> put_flash(:info, "Nutzer erfolgreich entfernt.")
            |> redirect(to: cms_committee_path(conn, :show, committee.id))

          _ ->
            conn
            |> put_flash(:error, "Fehler beim Entfernen")
            |> redirect(to: cms_committee_path(conn, :show, committee.id))
        end

      committee ->
        conn
        |> put_flash(:error, "Fehler beim Entfernen: Nutzer nicht gefunden.")
        |> redirect(to: cms_committee_path(conn, :show, committee.id))

      true ->
        conn
        |> put_flash(:error, "Fehler beim Entfernen: Komitee nicht gefunden")
        |> redirect(to: cms_committee_path(conn, :show, committee.id))

    end
  end
end
