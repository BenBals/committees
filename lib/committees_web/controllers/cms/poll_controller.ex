defmodule CommitteesWeb.CMS.PollController do
  use CommitteesWeb, :controller

  alias Committees.CMS
  alias Committees.CMS.Poll

  def index(conn, _params) do
    user_id = get_session(conn, :user_id)
    polls = CMS.list_polls_voted(user_id)
    IO.puts("========== Polls =============")
    IO.inspect(polls)
    render(conn, "index.html", polls: polls)
  end

  def overview(conn, _params) do
    polls = CMS.list_polls()
    render(conn, "overview.html", polls: polls)
  end

  def new(conn, _params) do
    changeset = CMS.create_change_poll(%Poll{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"poll" => poll_params}) do
    case CMS.create_poll(poll_params) do
      {:ok, poll} ->
        conn
        |> put_flash(:info, "Umfrage erstellt")
        |> redirect(to: cms_poll_path(conn, :show, poll))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    poll = CMS.get_poll!(id)
    render(conn, "show.html", poll: poll)
  end

  def edit(conn, %{"id" => id}) do
    poll = CMS.get_poll!(id)
    changeset = CMS.update_change_poll(poll)
    render(conn, "edit.html", poll: poll, changeset: changeset)
  end

  def update(conn, %{"id" => id, "poll" => poll_params}) do
    poll = CMS.get_poll!(id)

    case CMS.update_poll(poll, poll_params) do
      {:ok, poll} ->
        conn
        |> put_flash(:info, "Umfrage gespeichert")
        |> redirect(to: cms_poll_path(conn, :show, poll))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", poll: poll, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    poll = CMS.get_poll!(id)
    {:ok, _poll} = CMS.delete_poll(poll)

    conn
    |> put_flash(:info, "Umfrage erfolgreich gelöscht")
    |> redirect(to: cms_poll_path(conn, :index))
  end

  def cast_vote(conn, %{"id" => poll_id, "vote" => %{"option_id" => option_id}}) do
    user = conn.assigns[:current_user]

    if Poll.has_user_voted?(user, poll_id) do
      conn
      |> put_flash(:error, "In dieser Umfrage hast du bereits abgestimmt.")
      |> redirect(to: cms_poll_path(conn, :show, poll_id))
    else
      attrs = (%{option: option_id, poll_id: poll_id, user_id: user.id})

      case CMS.create_vote(attrs) do
        {:ok, _} ->
          conn
          |> put_flash(:info, "Du hast deine Stimme erfolgreich abgegeben.")
          |> redirect(to: cms_poll_path(conn, :show, poll_id))
        _ ->
          conn
          |> put_flash(:error, "Es gab einen Fehler bei der Stimmabgabe. Bitte versuch es nochmal.")
          |> redirect(to: cms_poll_path(conn, :edit, poll_id))
      end
    end
  end
end
