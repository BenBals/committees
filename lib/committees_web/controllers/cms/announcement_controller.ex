defmodule CommitteesWeb.CMS.AnnouncementController do
  use CommitteesWeb, :controller

  alias Committees.CMS
  alias Committees.CMS.Announcement
  alias CommitteesWeb.AuthHelpers

  def index(conn, _params) do
    announcements = CMS.list_announcements()
    render(conn, "index.html", announcements: announcements)
  end

  def new(conn, _params) do
    changeset = CMS.change_announcement(%Announcement{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"announcement" => %{"committe_id" => committee_id} = announcement_params}) do
    conn = AuthHelpers.members_and_admins_only(conn, committee_id)

    if conn.halted do
      conn
    else
      case CMS.create_announcement(announcement_params) do
        {:ok, announcement} ->
          conn
          |> put_flash(:info, "Announcement created successfully.")
          |> redirect(to: cms_announcement_path(conn, :show, announcement))
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "new.html", changeset: changeset)
      end
    end
  end

  def create(conn, params) do
    conn
    |> put_flash(:error, "Komitee muss gesetzt sein.")
    |> new(params)
  end

  def show(conn, %{"id" => id}) do
    announcement = CMS.get_announcement!(id)
    render(conn, "show.html", announcement: announcement)
  end

  def edit(conn, %{"id" => id}) do
    announcement = CMS.get_announcement!(id)
    conn = AuthHelpers.members_and_admins_only(conn, announcement.committee)
    changeset = CMS.change_announcement(announcement)
    render(conn, "edit.html", announcement: announcement, changeset: changeset)
  end

  def update(conn, %{"id" => id, "announcement" => announcement_params}) do
    announcement = CMS.get_announcement!(id)

    conn = AuthHelpers.members_and_admins_only(conn, announcement.committee)
    if conn.halted do
      conn
    else
      case CMS.update_announcement(announcement, announcement_params) do
        {:ok, announcement} ->
          conn
          |> put_flash(:info, "Announcement updated successfully.")
          |> redirect(to: cms_announcement_path(conn, :show, announcement))
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "edit.html", announcement: announcement, changeset: changeset)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    announcement = CMS.get_announcement!(id)
    conn = AuthHelpers.members_and_admins_only(conn, announcement.committee)

    if conn.halted do
      conn
    else
      {:ok, _announcement} = CMS.delete_announcement(announcement)

      conn
      |> put_flash(:info, "Announcement deleted successfully.")
      |> redirect(to: cms_announcement_path(conn, :index))
    end

  end
end
