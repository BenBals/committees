defmodule CommitteesWeb.UserController do
  use CommitteesWeb, :controller

  alias Committees.Accounts
  alias Committees.Accounts.User

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Nutzer erstellt")
        |> redirect(to: user_path(conn, :show, user))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def edit_self(conn, _) do
    user = conn.assigns[:current_user]

    case user do
      %User{} = user ->
        changeset = Accounts.change_user(user)
        render(conn, "edit_self.html", user: user, changeset: changeset)
      nil ->
        conn
        |> put_flash(:error, "Diese Funktion ist zur für angemeldete Nutzer verfügbar")
        |> redirect(to: session_path(conn, :new))
    end
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Nutzer gespeichert")
        |> redirect(to: user_path(conn, :show, user))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def update_self(conn, %{"user" => user_params}) do
    user = conn.assigns[:current_user]

    case Accounts.update_user(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Profil erfolgreich bearbeitet.")
        |> redirect(to: page_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit_self.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    {:ok, _user} = Accounts.delete_user(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: user_path(conn, :index))
  end

  def batch_create(conn, %{"data" => data, "admin_password" => pass}) do
    IO.puts "Admin Password"
    IO.inspect(admin_password())
    if (admin_password() != "" && admin_password() == pass) do
      result = Accounts.batch_create_users(data)
      case result do
        {:ok, message} ->
          conn
          |> put_status(:created)
          |> render("success.json", message: message)
        {:error, message} ->
          conn
          |> put_status(:unprocessable_entity)
          |> render(CommitteesWeb.ErrorView, "error.json", message: message)
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render(CommitteesWeb.ErrorView, "error.json", message: "invalid admin password")
    end
  end

  defp admin_password do
    {mix_exists, _} = Code.ensure_loaded(Mix)
    if (mix_exists == :error) do
      System.get_env("ADMIN_PASSWORD")
    else
      if Mix.env === :test do
        "TEST"
      else
        System.get_env("ADMIN_PASSWORD")
      end
    end
  end
end
