defmodule CommitteesWeb.Router do
  use CommitteesWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :authenticate_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :admin_only do
    plug :check_user_role, "admin"
  end

  scope "/", CommitteesWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    resources "/sessions", SessionController, only: [:new, :create, :delete], singleton: true

    pipe_through [:logged_in_users_only]
    get "/users/me/edit", UserController, :edit_self
    patch "/users/me/edit", UserController, :update_self
    put "/users/me/edit", UserController, :update_self

    pipe_through [:admin_only]
    resources "/users", UserController

  end

  scope "/cms", CommitteesWeb.CMS, as: :cms do
    pipe_through :browser
    resources "/announcements", AnnouncementController, only: [:index, :show]
  end

  scope "/cms", CommitteesWeb.CMS, as: :cms do
    pipe_through [:browser, :logged_in_users_only, :admin_only]

    get "/polls/overview", PollController, :overview
    resources "/committees", CommitteeController, only: [:edit, :new, :create, :update, :delete]
    delete "/committees/:id/users/:user_id", CommitteeController, :remove_user
    get "/committees/:id/users/new", CommitteeController, :new_user
    post "/committees/:id/users/", CommitteeController, :create_user

  end

  scope "/cms", CommitteesWeb.CMS, as: :cms do
    pipe_through [:browser, :logged_in_users_only]

    resources "/committees", CommitteeController, only: [:index, :show]
    resources "/announcements", AnnouncementController, except: [:index, :show]
    resources "/polls", PollController

    post "/polls/:id/votes", PollController, :cast_vote
  end

  scope "/api", CommitteesWeb do
    pipe_through :api

    post "/users/batch_create", UserController, :batch_create
  end

  defp authenticate_user(conn, _params) do
    case get_session(conn, :user_id) do
      nil ->
        conn
      user_id ->
        user = Committees.Accounts.get_user!(user_id)
        assign(conn, :current_user, user)
    end
  end

  defp logged_in_users_only(conn, params) do
    if conn.assigns[:current_user] do
      conn
    else
      conn
      |> Phoenix.Controller.put_flash(:error, "Dieser Bereich ist nur für angemeldete Nutzer verfügbar.")
      |> Phoenix.Controller.redirect(to: "/")
      |> halt()
    end
  end

  defp check_user_role(conn, role) do
    if conn.assigns[:current_user].role == role do
      conn
    else
      conn
      |> Phoenix.Controller.put_flash(:error, "Du hast nicht die nötige Berechtigung.")
      |> Phoenix.Controller.redirect(to: "/")
      |> halt()
    end
  end

end
