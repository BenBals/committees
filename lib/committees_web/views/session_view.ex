defmodule CommitteesWeb.SessionView do
  use CommitteesWeb, :view

  def logged_in(conn) do
    case Plug.Conn.get_session(conn, :user_id) do
      nil -> false
      _ -> true
    end
  end
end
