defmodule CommitteesWeb.ViewHelpers do
  def user_committees_for_select(conn) do
    conn.assigns[:current_user].committees
    |> Enum.map(fn committee -> {committee.name, committee.id} end)
  end
end
