defmodule CommitteesWeb.CMS.PollView do
  use CommitteesWeb, :view

  def has_user_voted?(conn, poll_id) do
    user = conn.assigns[:current_user]
    Committees.CMS.Poll.has_user_voted?(user, poll_id)
  end

  def select_options(poll), do: Committees.CMS.Poll.options_by_id(poll)

end
