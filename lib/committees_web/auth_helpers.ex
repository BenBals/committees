defmodule CommitteesWeb.AuthHelpers do
  def member_of_committee?(conn, committee_id) when is_integer(committee_id) do
    committee_ids = conn.assigns[:current_user].committees |> Enum.map(fn x -> x.id end)

    committee_id in committee_ids
  end

  def member_of_committee?(conn, committee) do
    committees = conn.assigns[:current_user].committees

    committee in committees
  end

  def can_edit_committee?(conn, committee) do
    admin? = conn.assigns[:current_user].role == "admin"
    member? = member_of_committee?(conn, committee)

    admin? || member?
  end

  def members_and_admins_only(conn, committee) do
    if can_edit_committee?(conn, committee) do
      conn
    else
      conn
      |> Phoenix.Controller.put_flash(:error, "Nur Mitglieder des Komitees haben hier Zugriff")
      |> Phoenix.Controller.redirect(to: "/")
      |> Plug.Conn.halt()
    end
  end
end
