defmodule Committees.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Committees.Repo

  alias Committees.Accounts.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    query = from User,
      order_by: :name

    Repo.all(query)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id) |> Repo.preload([:committees, :votes])

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def batch_create_users(list_of_attrs) do
    changesets = Enum.map(list_of_attrs, fn attrs ->  User.registration_changeset(%User{}, attrs) end)
    all_valid = Enum.all?(changesets, fn x -> x.valid? end)

    if all_valid do

      result = Repo.transaction(fn -> insert_users(changesets) end)
      case result do
        {:ok, _} -> {:ok, "users created successfully"}
        {:error, _} -> {:error, "invalid user data"}
      end

    else
      {:error, "invalid user data"}
    end
  end

  defp insert_users(users), do: Enum.map(users, &Repo.insert!/1)

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.registration_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  def authenticate_by_username_password(username, password) do
    cleaned_username = username |> safe_downcase() |> safe_trim()
    user  = Repo.get_by(User, username: cleaned_username)

    cond do
      user && Comeonin.Bcrypt.checkpw(password, user.password_hash) ->
        {:ok, user}
      user ->
        {:error, :unauthorized}
      true ->
        Comeonin.Bcrypt.dummy_checkpw()
        {:error, :unauthorized}
    end
  end

  defp safe_downcase(string) when is_binary(string), do: String.downcase(string)
  defp safe_downcase(_), do: nil

  defp safe_trim(string) when is_binary(string), do: String.trim(string)
  defp safe_trim(_), do: nil
end
