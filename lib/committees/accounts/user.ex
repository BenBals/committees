defmodule Committees.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Committees.Accounts.User


  schema "users" do
    field :name, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :username, :string
    field :role, :string

    many_to_many :committees, Committees.CMS.Committee, join_through: "users_committees", on_delete: :delete_all
    has_many :votes, Committees.CMS.Vote, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:name, :username, :role])
    |> validate_required([:name, :username])
    |> validate_inclusion(:role, [nil, "admin"])
    |> update_change(:username, &String.downcase/1)
    |> unique_constraint(:username)
  end

  def registration_changeset(user, attrs \\ %{}) do
    user
    |> changeset(attrs)
    |> cast(attrs, [:password])
    |> validate_length(:password, min: 8, max: 200)
    |> put_password_hash()
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(password))
      _ ->
        changeset
    end
  end
end
