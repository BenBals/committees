defmodule Committees.CMS.Committee do
  use Ecto.Schema
  import Ecto.Changeset
  alias Committees.CMS.Committee


  schema "committees" do
    field :name, :string
    field :shortname, :string

    many_to_many :users, Committees.Accounts.User, join_through: "users_committees", on_delete: :delete_all
    has_many :announcements, Committees.CMS.Announcement, on_delete: :delete_all
    has_many :polls, Committees.CMS.Poll, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(%Committee{} = committee, attrs) do
    committee
    |> cast(attrs, [:name, :shortname])
    |> validate_required([:name, :shortname])
    |> unique_constraint(:shortname)
  end

  def set_users_changeset(committee, users) do
    committee
    |> changeset(%{})
    |> put_assoc(:users, users)
  end
end
