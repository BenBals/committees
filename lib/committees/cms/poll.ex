defmodule Committees.CMS.Poll do
  use Ecto.Schema
  import Ecto.Changeset
  alias Committees.CMS.Poll
  alias Committees.Accounts


  schema "polls" do
    field :description, :string
    field :end_date, :naive_datetime
    field :name, :string
    field :options, :string
    belongs_to :committee, Committees.CMS.Committee
    has_many :votes, Committees.CMS.Vote, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def create_changeset(%Poll{} = poll, attrs) do
    poll
    |> Map.put_new(:options, "")
    |> cast(attrs, [:name, :description, :options, :end_date, :committee_id])
    |> IO.inspect
    |> update_change(:options, &replace_empty_option_with_all_students/1)
    |> validate_required([:name, :description, :options, :end_date, :committee_id])
  end

  def update_changeset(%Poll{} = poll, attrs) do
    poll
    |> cast(attrs, [:name, :description, :end_date, :committee_id])
    |> validate_required([:name, :description, :options, :end_date])
  end

  def has_user_voted?(user, %Poll{} = poll), do: has_user_voted?(user, poll.id)

  def has_user_voted?(user, poll_id) when is_binary(poll_id) do
    has_user_voted?(user, String.to_integer(poll_id))
  end

  def has_user_voted?(user, poll_id) when is_integer(poll_id) do
    poll_id in (user.votes |> Enum.map(fn vote -> vote.poll_id end))
  end

  def has_poll_ended?(poll) do
    NaiveDateTime.compare(poll.end_date, NaiveDateTime.utc_now() |> NaiveDateTime.add(2 * 3600, :second)) === :lt
  end

  @doc """
  Gives the options and votes for a poll in a list of tuples
  [{"name", id, num_of_votes}]
  """
  def options_and_votes(poll) do
    options_by_id(poll)
    |> Enum.map(fn {name, id} ->
      {name,
       id,
       poll.votes
       |> Enum.filter(fn vote -> vote.option === id end)
       |> length
      }
    end)
    |> Enum.filter(fn {_name, _id, votes} -> votes > 0 end)
    |> Enum.sort_by(fn {_name, _id, votes} -> votes end, &>=/2)
  end

  @doc """
  Gives a list of Strings representing the winners in order.
  E. g. ["alice", "bob, charlie", "dave"] would mean alice came in first, bob and charlie second and dave third
  """
  def winner_list(poll) do
    options_and_votes = options_and_votes(poll)

    options_and_votes
    |> Enum.map(fn {_name, _id, votes} -> votes end)
    |> Enum.sort()
    |> Enum.dedup()
    |> Enum.reverse()
    |> Enum.map(fn votes ->
      Enum.filter(options_and_votes, fn {_, _, votes_inner} -> votes === votes_inner end)
      |> Enum.map(fn {name, _, _} -> name end )
    end)
    |> Enum.map(fn list -> Enum.join(list, ", ") end)
  end

  @doc """
  Gives back the options and their ids in a list of tuples: [{"name", id}]
  """
  def options_by_id(poll) do
    options =
      poll.options
      |> String.split("\n")
      |> Enum.map(&String.trim/1)
      |> Enum.reduce({0, []}, fn(x, {id, list}) -> {id+1, list++[{x, id}]} end)
      |> extract_from_reduce_acc()
  end
  defp extract_from_reduce_acc({_, list}), do: list

  defp replace_empty_option_with_all_students("") do
    Accounts.list_users()
    |> Enum.map(fn user -> user.name end)
    |> Enum.join(",")
  end

  defp replace_empty_option_with_all_students(options), do: options
end
