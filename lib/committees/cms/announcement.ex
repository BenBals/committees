defmodule Committees.CMS.Announcement do
  use Ecto.Schema
  import Ecto.Changeset
  alias Committees.CMS.Announcement


  schema "announcements" do
    field :body, :string
    field :summary, :string
    field :title, :string
    belongs_to :committee, Committees.CMS.Committee

    timestamps()
  end

  @doc false
  def changeset(%Announcement{} = announcement, attrs) do
    announcement
    |> cast(attrs, [:title, :body, :summary, :committee_id])
    |> validate_required([:title, :body, :summary, :committee_id])
  end
end
