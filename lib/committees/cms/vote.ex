defmodule Committees.CMS.Vote do
  use Ecto.Schema
  import Ecto.Changeset
  alias Committees.CMS.Vote


  schema "votes" do
    field :option, :integer
    belongs_to :poll, Committees.CMS.Poll
    belongs_to :user, Committees.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(%Vote{} = vote, attrs) do
    vote
    |> cast(attrs, [:option, :user_id, :poll_id])
    |> validate_required([:option, :user_id, :poll_id])
  end
end
