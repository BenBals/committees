defmodule Committees.CMS do
  @moduledoc """
  The CMS context.
  """

  import Ecto.Query, warn: false
  alias Committees.Repo

  alias Committees.CMS.Committee

  def list_committees do
    Repo.all(Committee)
  end

  def get_committee!(id) do
    Repo.get!(Committee, id)
    |> Repo.preload([:users, :announcements, :polls])
  end

  def create_committee(attrs \\ %{}) do
    %Committee{}
    |> Committee.changeset(attrs)
    |> Repo.insert()
  end

  def update_committee(%Committee{} = committee, attrs) do
    committee
    |> Committee.changeset(attrs)
    |> Repo.update()
  end

  def delete_committee(%Committee{} = committee) do
    Repo.delete(committee)
  end

  def change_committee(%Committee{} = committee) do
    Committee.changeset(committee, %{})
  end

  def remove_user_from_committee(user, committee) do
    query = from uc in "users_committees",
      where: uc.user_id == ^user.id and uc.committee_id == ^committee.id

    query
    |> Repo.delete_all()
  end

  def add_user_to_committee(user, committee) do
    committee = committee |> Repo.preload(:users)
    changeset = Committee.set_users_changeset(committee, committee.users ++ [user])

    Repo.update(changeset)
  end

  alias Committees.CMS.Announcement

  @doc """
  Returns the list of announcements.

  ## Examples

      iex> list_announcements()
      [%Announcement{}, ...]

  """
  def list_announcements do
    Repo.all(Announcement) |> Repo.preload(:committee)
  end

  @doc """
  Gets a single announcement.

  Raises `Ecto.NoResultsError` if the Announcement does not exist.

  ## Examples

      iex> get_announcement!(123)
      %Announcement{}

      iex> get_announcement!(456)
      ** (Ecto.NoResultsError)

  """
  def get_announcement!(id), do: Repo.get!(Announcement, id) |> Repo.preload(:committee)

  @doc """
  Creates a announcement.

  ## Examples

      iex> create_announcement(%{field: value})
      {:ok, %Announcement{}}

      iex> create_announcement(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_announcement(attrs \\ %{}) do
    %Announcement{}
    |> Announcement.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a announcement.

  ## Examples

      iex> update_announcement(announcement, %{field: new_value})
      {:ok, %Announcement{}}

      iex> update_announcement(announcement, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_announcement(%Announcement{} = announcement, attrs) do
    announcement
    |> Announcement.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Announcement.

  ## Examples

      iex> delete_announcement(announcement)
      {:ok, %Announcement{}}

      iex> delete_announcement(announcement)
      {:error, %Ecto.Changeset{}}

  """
  def delete_announcement(%Announcement{} = announcement) do
    Repo.delete(announcement)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking announcement changes.

  ## Examples

      iex> change_announcement(announcement)
      %Ecto.Changeset{source: %Announcement{}}

  """
  def change_announcement(%Announcement{} = announcement) do
    Announcement.changeset(announcement, %{})
  end

  alias Committees.CMS.Poll

  def get_poll!(id), do: Repo.get!(Poll, id) |> Repo.preload([:committee, :votes])

  def list_polls do
    Repo.all(Poll)
    |> Repo.preload([:committee, :votes])
  end

  def list_polls_voted(user_id) do
    IO.puts("user_id #{user_id}")
    list_polls()
    |> Repo.preload([:votes])
    |> Enum.map(fn poll ->
      {poll, Enum.any?(poll.votes, fn vote -> vote.user_id == user_id end)}
    end)
    |> Enum.sort(fn ({_poll1, voted1}, {_poll2, voted2}) ->
      (voted2 and not voted1)
    end)
  end

  def create_poll(attrs \\ %{}) do
    %Poll{}
    |> Poll.create_changeset(attrs)
    |> Repo.insert()
  end

  def update_poll(%Poll{} = poll, attrs) do
    poll
    |> Poll.update_changeset(attrs)
    |> Repo.update()
  end

  def delete_poll(%Poll{} = poll) do
    Repo.delete(poll)
  end

  def create_change_poll(%Poll{} = poll) do
    Poll.create_changeset(poll, %{})
  end

  def update_change_poll(%Poll{} = poll) do
    Poll.update_changeset(poll, %{})
  end

  alias Committees.CMS.Vote

  @doc """
  Returns the list of votes.

  ## Examples

      iex> list_votes()
      [%Vote{}, ...]

  """
  def list_votes do
    Repo.all(Vote)
  end

  @doc """
  Gets a single vote.

  Raises `Ecto.NoResultsError` if the Vote does not exist.

  ## Examples

      iex> get_vote!(123)
      %Vote{}

      iex> get_vote!(456)
      ** (Ecto.NoResultsError)

  """
  def get_vote!(id), do: Repo.get!(Vote, id)

  @doc """
  Creates a vote.

  ## Examples

      iex> create_vote(%{field: value})
      {:ok, %Vote{}}

      iex> create_vote(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_vote(attrs \\ %{}) do
    %Vote{}
    |> Vote.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a vote.

  ## Examples

      iex> update_vote(vote, %{field: new_value})
      {:ok, %Vote{}}

      iex> update_vote(vote, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_vote(%Vote{} = vote, attrs) do
    vote
    |> Vote.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Vote.

  ## Examples

      iex> delete_vote(vote)
      {:ok, %Vote{}}

      iex> delete_vote(vote)
      {:error, %Ecto.Changeset{}}

  """
  def delete_vote(%Vote{} = vote) do
    Repo.delete(vote)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking vote changes.

  ## Examples

      iex> change_vote(vote)
      %Ecto.Changeset{source: %Vote{}}

  """
  def change_vote(%Vote{} = vote) do
    Vote.changeset(vote, %{})
  end
end
