FROM ubuntu
RUN apt-get update && \
    apt-get install -y libssl1.0.0 postgresql-client locales && \
    apt-get autoclean

RUN mkdir -p /app
ARG VERSION=0.0.3
COPY _build/prod/rel/committees/releases/${VERSION}/committees.tar.gz /app/committees.tar.gz
COPY scripts/wait-for-postgres.sh /app/wait-for-postgres.sh
COPY scripts/on-postgres-up.sh /app/on-postgres-up.sh
WORKDIR /app
RUN tar xvfz committees.tar.gz
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV PORT 8888
CMD ["/app/bin/committees", "foreground"]