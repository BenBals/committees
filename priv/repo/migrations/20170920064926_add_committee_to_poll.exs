defmodule Committees.Repo.Migrations.AddCommitteeToPoll do
  use Ecto.Migration

  def change do
    alter table(:polls) do
      add :committee_id, references(:committees, on_delete: :nothing)
    end
  end
end
