defmodule Committees.Repo.Migrations.CreateAnnouncements do
  use Ecto.Migration

  def change do
    create table(:announcements) do
      add :title, :string
      add :body, :text
      add :summary, :text
      add :committee_id, references(:committees, on_delete: :nothing)

      timestamps()
    end

    create index(:announcements, [:committee_id])
  end
end
