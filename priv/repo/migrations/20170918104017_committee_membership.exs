defmodule Committees.Repo.Migrations.CommitteeMembership do
  use Ecto.Migration

  def change do
    create table(:users_committees, primary_key: false) do
      add :user_id, references(:users)
      add :committee_id, references(:committees)
    end
  end
end
