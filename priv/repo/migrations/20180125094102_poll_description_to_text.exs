defmodule Committees.Repo.Migrations.PollDescriptionToText do
  use Ecto.Migration

  def change do
    alter table(:polls) do
      modify :description, :text
    end
  end
end
