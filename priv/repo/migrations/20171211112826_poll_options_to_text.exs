defmodule Committees.Repo.Migrations.PollOptionsToText do
  use Ecto.Migration

  def change do
    alter table(:polls) do
      modify :options, :text
    end
  end
end
