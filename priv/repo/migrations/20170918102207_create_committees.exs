defmodule Committees.Repo.Migrations.CreateCommittees do
  use Ecto.Migration

  def change do
    create table(:committees) do
      add :name, :string
      add :shortname, :string

      timestamps()
    end

    create unique_index(:committees, [:shortname])
  end
end
