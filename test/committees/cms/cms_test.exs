defmodule Committees.CMSTest do
  use Committees.DataCase

  alias Committees.CMS

  describe "committees" do
    alias Committees.CMS.Committee

    @valid_attrs %{name: "some name", shortname: "some shortname"}
    @update_attrs %{name: "some updated name", shortname: "some updated shortname"}
    @invalid_attrs %{name: nil, shortname: nil}

    def committee_fixture(attrs \\ %{}) do
      {:ok, committee} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CMS.create_committee()

      committee
    end

    test "list_committees/0 returns all committees" do
      committee = committee_fixture()
      assert CMS.list_committees() == [committee]
    end

    test "get_committee!/1 returns the committee with given id" do
      committee = committee_fixture()
      assert CMS.get_committee!(committee.id) == committee
    end

    test "create_committee/1 with valid data creates a committee" do
      assert {:ok, %Committee{} = committee} = CMS.create_committee(@valid_attrs)
      assert committee.name == "some name"
      assert committee.shortname == "some shortname"
    end

    test "create_committee/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_committee(@invalid_attrs)
    end

    test "update_committee/2 with valid data updates the committee" do
      committee = committee_fixture()
      assert {:ok, committee} = CMS.update_committee(committee, @update_attrs)
      assert %Committee{} = committee
      assert committee.name == "some updated name"
      assert committee.shortname == "some updated shortname"
    end

    test "update_committee/2 with invalid data returns error changeset" do
      committee = committee_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_committee(committee, @invalid_attrs)
      assert committee == CMS.get_committee!(committee.id)
    end

    test "delete_committee/1 deletes the committee" do
      committee = committee_fixture()
      assert {:ok, %Committee{}} = CMS.delete_committee(committee)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_committee!(committee.id) end
    end

    test "change_committee/1 returns a committee changeset" do
      committee = committee_fixture()
      assert %Ecto.Changeset{} = CMS.change_committee(committee)
    end
  end

  describe "announcements" do
    alias Committees.CMS.Announcement

    @valid_attrs %{body: "some body", summary: "some summary", title: "some title"}
    @update_attrs %{body: "some updated body", summary: "some updated summary", title: "some updated title"}
    @invalid_attrs %{body: nil, summary: nil, title: nil}

    def announcement_fixture(attrs \\ %{}) do
      {:ok, announcement} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CMS.create_announcement()

      announcement
    end

    test "list_announcements/0 returns all announcements" do
      announcement = announcement_fixture()
      assert CMS.list_announcements() == [announcement]
    end

    test "get_announcement!/1 returns the announcement with given id" do
      announcement = announcement_fixture()
      assert CMS.get_announcement!(announcement.id) == announcement
    end

    test "create_announcement/1 with valid data creates a announcement" do
      assert {:ok, %Announcement{} = announcement} = CMS.create_announcement(@valid_attrs)
      assert announcement.body == "some body"
      assert announcement.summary == "some summary"
      assert announcement.title == "some title"
    end

    test "create_announcement/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_announcement(@invalid_attrs)
    end

    test "update_announcement/2 with valid data updates the announcement" do
      announcement = announcement_fixture()
      assert {:ok, announcement} = CMS.update_announcement(announcement, @update_attrs)
      assert %Announcement{} = announcement
      assert announcement.body == "some updated body"
      assert announcement.summary == "some updated summary"
      assert announcement.title == "some updated title"
    end

    test "update_announcement/2 with invalid data returns error changeset" do
      announcement = announcement_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_announcement(announcement, @invalid_attrs)
      assert announcement == CMS.get_announcement!(announcement.id)
    end

    test "delete_announcement/1 deletes the announcement" do
      announcement = announcement_fixture()
      assert {:ok, %Announcement{}} = CMS.delete_announcement(announcement)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_announcement!(announcement.id) end
    end

    test "change_announcement/1 returns a announcement changeset" do
      announcement = announcement_fixture()
      assert %Ecto.Changeset{} = CMS.change_announcement(announcement)
    end
  end

  describe "polls" do
    alias Committees.CMS.Poll

    @valid_attrs %{description: "some description", end_date: ~N[2010-04-17 14:00:00.000000], name: "some name", options: "some options"}
    @update_attrs %{description: "some updated description", end_date: ~N[2011-05-18 15:01:01.000000], name: "some updated name", options: "some updated options"}
    @invalid_attrs %{description: nil, end_date: nil, name: nil, options: nil}

    def poll_fixture(attrs \\ %{}) do
      {:ok, poll} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CMS.create_poll()

      poll
    end

    test "list_polls/0 returns all polls" do
      poll = poll_fixture()
      assert CMS.list_polls() == [poll]
    end

    test "get_poll!/1 returns the poll with given id" do
      poll = poll_fixture()
      assert CMS.get_poll!(poll.id) == poll
    end

    test "create_poll/1 with valid data creates a poll" do
      assert {:ok, %Poll{} = poll} = CMS.create_poll(@valid_attrs)
      assert poll.description == "some description"
      assert poll.end_date == ~N[2010-04-17 14:00:00.000000]
      assert poll.name == "some name"
      assert poll.options == "some options"
    end

    test "create_poll/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_poll(@invalid_attrs)
    end

    test "update_poll/2 with valid data updates the poll" do
      poll = poll_fixture()
      assert {:ok, poll} = CMS.update_poll(poll, @update_attrs)
      assert %Poll{} = poll
      assert poll.description == "some updated description"
      assert poll.end_date == ~N[2011-05-18 15:01:01.000000]
      assert poll.name == "some updated name"
      assert poll.options == "some updated options"
    end

    test "update_poll/2 with invalid data returns error changeset" do
      poll = poll_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_poll(poll, @invalid_attrs)
      assert poll == CMS.get_poll!(poll.id)
    end

    test "delete_poll/1 deletes the poll" do
      poll = poll_fixture()
      assert {:ok, %Poll{}} = CMS.delete_poll(poll)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_poll!(poll.id) end
    end

    test "change_poll/1 returns a poll changeset" do
      poll = poll_fixture()
      assert %Ecto.Changeset{} = CMS.change_poll(poll)
    end
  end

  describe "votes" do
    alias Committees.CMS.Vote

    @valid_attrs %{option: 42}
    @update_attrs %{option: 43}
    @invalid_attrs %{option: nil}

    def vote_fixture(attrs \\ %{}) do
      {:ok, vote} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CMS.create_vote()

      vote
    end

    test "list_votes/0 returns all votes" do
      vote = vote_fixture()
      assert CMS.list_votes() == [vote]
    end

    test "get_vote!/1 returns the vote with given id" do
      vote = vote_fixture()
      assert CMS.get_vote!(vote.id) == vote
    end

    test "create_vote/1 with valid data creates a vote" do
      assert {:ok, %Vote{} = vote} = CMS.create_vote(@valid_attrs)
      assert vote.option == 42
    end

    test "create_vote/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_vote(@invalid_attrs)
    end

    test "update_vote/2 with valid data updates the vote" do
      vote = vote_fixture()
      assert {:ok, vote} = CMS.update_vote(vote, @update_attrs)
      assert %Vote{} = vote
      assert vote.option == 43
    end

    test "update_vote/2 with invalid data returns error changeset" do
      vote = vote_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_vote(vote, @invalid_attrs)
      assert vote == CMS.get_vote!(vote.id)
    end

    test "delete_vote/1 deletes the vote" do
      vote = vote_fixture()
      assert {:ok, %Vote{}} = CMS.delete_vote(vote)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_vote!(vote.id) end
    end

    test "change_vote/1 returns a vote changeset" do
      vote = vote_fixture()
      assert %Ecto.Changeset{} = CMS.change_vote(vote)
    end
  end
end
