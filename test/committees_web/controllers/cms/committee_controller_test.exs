defmodule CommitteesWeb.CMS.CommitteeControllerTest do
  use CommitteesWeb.ConnCase

  alias Committees.CMS

  @create_attrs %{name: "some name", shortname: "some shortname"}
  @update_attrs %{name: "some updated name", shortname: "some updated shortname"}
  @invalid_attrs %{name: nil, shortname: nil}

  def fixture(:committee) do
    {:ok, committee} = CMS.create_committee(@create_attrs)
    committee
  end

  describe "index" do
    test "lists all committees", %{conn: conn} do
      conn = get conn, cms_committee_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Committees"
    end
  end

  describe "new committee" do
    test "renders form", %{conn: conn} do
      conn = get conn, cms_committee_path(conn, :new)
      assert html_response(conn, 200) =~ "New Committee"
    end
  end

  describe "create committee" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, cms_committee_path(conn, :create), committee: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == cms_committee_path(conn, :show, id)

      conn = get conn, cms_committee_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Committee"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, cms_committee_path(conn, :create), committee: @invalid_attrs
      assert html_response(conn, 200) =~ "New Committee"
    end
  end

  describe "edit committee" do
    setup [:create_committee]

    test "renders form for editing chosen committee", %{conn: conn, committee: committee} do
      conn = get conn, cms_committee_path(conn, :edit, committee)
      assert html_response(conn, 200) =~ "Edit Committee"
    end
  end

  describe "update committee" do
    setup [:create_committee]

    test "redirects when data is valid", %{conn: conn, committee: committee} do
      conn = put conn, cms_committee_path(conn, :update, committee), committee: @update_attrs
      assert redirected_to(conn) == cms_committee_path(conn, :show, committee)

      conn = get conn, cms_committee_path(conn, :show, committee)
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, committee: committee} do
      conn = put conn, cms_committee_path(conn, :update, committee), committee: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Committee"
    end
  end

  describe "delete committee" do
    setup [:create_committee]

    test "deletes chosen committee", %{conn: conn, committee: committee} do
      conn = delete conn, cms_committee_path(conn, :delete, committee)
      assert redirected_to(conn) == cms_committee_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, cms_committee_path(conn, :show, committee)
      end
    end
  end

  defp create_committee(_) do
    committee = fixture(:committee)
    {:ok, committee: committee}
  end
end
